var App = {
    selectSizeBtn: function () {
        var btnSelector = $('.product-desc ul li');
        btnSelector.click(function () {
            btnSelector.removeClass('active');
            $(this).addClass('active');
            var btnValue = $(this).attr('title');
            $('.size-display').html(btnValue);
        });
    },
    cartItemsBtn: function () {
        var cartItemsCounter = $('.cart-items .item').length;
        $('.item-counter').html(cartItemsCounter);
    },
    addToCartBtn: function () {
        var obj = this;
        var systemMessage = $('.system-message');
        $('.add-to-cart-btn').click(function () {
            var validator = $('.size-display');
            if (validator.html() !== '') {
                var size = $('.size-display').html();
                $('.cart-items .empty').remove();
                if ($('.cart-items li').hasClass('item-' + size)) {
                    var qtyWrapper = $('.item-' + size + ' .qty');
                    var currentCount = qtyWrapper.html();
                    currentCount = parseInt(currentCount) + 1;
                    qtyWrapper.html(currentCount);
                } else {
                    $('.cart-items').prepend('<li class="item item-' + size + '">'
                            + '<div class="item-thumbnail">'
                            + '<img src="resources/img/classic-tee.jpg" />'
                            + '</div>'
                            + '<div class="item-desc">'
                            + '<div>Classic Tee</div>'
                            + '<div><span class="qty">1</span> x <strong>$75.00</strong></div>'
                            + '<div>Size: <span class="size">' + size + '</span></div>'
                            + '</div>'
                            + '</li>');
                }
                systemMessage.html('Classic Tee (' + size + ') successfully added to cart');
                obj.cartItemsBtn();
            } else {
                systemMessage.html('Please select size');
            }
            systemMessage.fadeIn('fast');
            setTimeout(function () {
                systemMessage.fadeOut('fast');
            }, 2000);
        });

    }
};

$(document).ready(function () {
    App.selectSizeBtn();
    App.addToCartBtn();
});